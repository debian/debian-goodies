=encoding UTF-8

=head1 NAME

find-dbgsym-packages - gets list of dbgsym packages from executable, library, core dump or process

=head1 SYNOPSIS

B<find-dbgsym-packages> [B<--install>] [B<--ssh>] [B<--deb>]
[B<--all>] I<executable, core file or PID> [I<executable, library,
core file or PID> …]

=head1 DESCRIPTION

B<find-dbgsym-packages> lists all I<*-dbgsym> packages required to
properly debug processes given by a list of executables, libraries, core dumps or PID numbers.

=head1 PREREQUISITES

You must already have the correct debug lines in your
I</etc/apt/sources.list> file, e.g.

    deb http://debug.mirrors.debian.org/debian-debug/ unstable-debug main

Don't forget to run C<apt update> as root after adding such a
line.

Additionally, B<find-dbgsym-packages> requires the I<elfutils> and
I<dctrl-tools> packages to be installed.

=head1 OPTIONS

=over 4

=item C<--install>

Don't display the list of required dbgsym packages but directly use
L<apt(1)> to install them. (Shows the apt command being executed,
though.) If not being run as root and L<sudo(1)> is installed, call
C<sudo apt install ...>, else use C<su - -c 'apt install ...'>.

=item C<--ssh>

Use C<ssh root@localhost -t> instead of C<sudo> or C<su - -c>. to gain
administrative privileges.

This requires the package C<openssh-client> and an SSH daemon
(e.g. C<openssh-server>) to be installed (and running).

=item C<--deb>

Generate a metapackage which depends on all the still needed debug symbol packages.

This requires the package C<equivs> to be installed.

If C<--install> is also given, automatically install the package and
all its (missing) dependencies.

=item C<--all>

Show or include I<all> relevant debug symbol packages, not just those
still missing.

=back

=head1 EXAMPLES

    $ find-dbgsym-packages 15865
    libcap2-dbgsym libtinfo5-dbg zsh-dbgsym
    $ find-dbgsym-packages /var/crash/1000/15865-1000-1000-11-1511059476-c6--bin-zsh.core
    libcap2-dbgsym libtinfo5-dbg zsh-dbgsym
    $ find-dbgsym-packages /bin/ls
    coreutils-dbgsym libpcre3-dbg libselinux1-dbgsym
    $ find-dbgsym-packages /lib/x86_64-linux-gnu/libselinux.so.1
    libpcre3-dbg libselinux1-dbgsym

=head1 AUTHOR

Stefan Fritch <L<sf@debian.org>>

This manpage was written by Axel Beckert <L<abe@debian.org>>.

=head1 COPYRIGHT AND LICENCE

Copyright (C) 2017 Stefan Fritch <L<sf@debian.org>>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

On Debian systems, a copy of the GNU General Public License version 2
can be found in F</usr/share/common-licenses/GPL-2>.

=head1 SEE ALSO

L<https://wiki.debian.org/AutomaticDebugPackages>,
L<debuginfod(8)>,
L<debuginfod-find(1)>
